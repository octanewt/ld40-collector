extends Node

var main_node setget , _get_main_node

func _get_main_node():
	var root = get_tree().get_root()
	return root.get_child(root.get_child_count()-1)

func find_node(node):
    return self.main_node.find_node(node)

func choose(choices):
    var i = randi() % choices.size()
    return choices[i]

func create_timer(wait_time):
    var timer = Timer.new()
    timer.set_wait_time(wait_time)
    timer.set_one_shot(true)
    timer.connect("timeout", timer, "queue_free")
    add_child(timer)
    timer.start()
    return timer

func attach(src, src_signal, trg, trg_func):
    if typeof(src) == TYPE_STRING:
        src = find_node(src)

    if typeof(trg) == TYPE_STRING:
        trg = find_node(trg)

    if src != null and trg != null:
        src.connect(src_signal, trg, trg_func)
