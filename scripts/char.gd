extends Area2D

var vel = Vector2()
var sprite_size = get_item_rect().size
export var speed = 500

func _ready():
	set_process(true)
	add_to_group("char")

func _process(delta):
	vel = Vector2()
	# handle the input
	if (Input.is_action_pressed("up")):
		vel += Vector2(0, -1)
	if (Input.is_action_pressed("down")):
		vel += Vector2(0, 1)
	if (Input.is_action_pressed("left")):
		vel += Vector2(-1, 0)
	if (Input.is_action_pressed("right")):
		vel += Vector2(1, 0)

	# do the moving
	vel = vel.normalized()
	set_pos(get_pos() + (vel * (speed * (1 - (utils.main_node.encumb * 0.7))) * delta))

	# clamping to view
	var pos = get_pos()
	pos.x = clamp(pos.x, 0 + sprite_size.width/2,  get_viewport_rect().size.width - sprite_size.width/2)
	pos.y = clamp(pos.y, 0 + sprite_size.height/2,  get_viewport_rect().size.height - sprite_size.height/2)
	set_pos(pos)
