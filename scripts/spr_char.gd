extends Sprite

func _ready():
	utils.attach("inventory", "inventory_changed", self, "_on_inventory_changed")

func _on_inventory_changed(amt):
	set_frame(amt)
