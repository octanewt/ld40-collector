extends Area2D

export var rock_timeout = 5

const rocks = [preload("res://scenes/diamond.tscn"),
			   preload("res://scenes/clay.tscn"),
			   preload("res://scenes/ruby.tscn")
			  ]

func _ready():
	set_process(true)
	add_to_group("boulder")
	connect("area_enter", self, "_on_area_enter")

func _process(delta):
	pass

func _on_area_enter(other):
	if other.is_in_group("char"):
		spawn_rocks()
		get_node("sprite").queue_free()
		get_node("shape").queue_free()

		var time_until_blink = floor(rock_timeout * 0.5)
		yield(utils.create_timer(time_until_blink), "timeout")

		for t in range(rock_timeout - time_until_blink):
			#print("Cur time: " + str(t))
			hide()
			for c in get_children():
				#print("Hiding: " + str(c))
				c.hide()
			yield(utils.create_timer(0.1), "timeout")

			show()
			for c in get_children():
				#print("Showing: " + str(c))
				c.show()
			yield(utils.create_timer(0.9), "timeout")

		queue_free()

func spawn_rocks():
	randomize()
	var rock = utils.choose(rocks).instance()
	add_child(rock)
