extends Area2D

export var val = 1

func _ready():
	connect("area_enter", self, "_on_area_enter")

func _on_area_enter(other):
	if other.is_in_group("char"):
		add_rock()

func add_rock():
	if utils.find_node("inventory").amt + 1 <= utils.find_node("inventory").maximum:
		utils.find_node("inventory").amt += 1
		utils.find_node("score").score += val

		utils.main_node.encumb = float(utils.find_node("inventory").amt) / utils.find_node("inventory").maximum
		queue_free()
