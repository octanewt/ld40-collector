extends TextureFrame

func _ready():
	set_process(true)
	get_node("amt").set_text(str(game.highscore))

func _process(delta):
	get_node("amt").set_text(str(game.highscore))