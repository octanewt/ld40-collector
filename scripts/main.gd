extends Node

export var max_inv = 3
var encumb = 0.0

func _ready():
	set_process(true)
	get_node("ui/inventory").set_max(max_inv)

func _process(delta):
	if (Input.is_action_pressed("restart")):
		get_tree().reload_current_scene()
	if (Input.is_action_pressed("quit")):
		get_tree().quit()
