extends TextureFrame

var score = 0 setget set_score

func _ready():
	pass

func set_score(val):
	score = val
	get_node("amt").set_text(str(score))

	if score > game.highscore:
		game.highscore = score
