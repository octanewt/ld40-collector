extends TextureFrame

var amt = 0 setget set_amt
var maximum = 0 setget set_max

signal inventory_changed

func _ready():
	pass

func set_amt(val):
	amt = val
	emit_signal("inventory_changed", amt)
	get_node("amt").set_text(str(amt))

func set_max(val):
	maximum = val
	get_node("maximum").set_text(str(maximum))
